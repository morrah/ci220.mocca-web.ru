<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Operations extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('operations_model');
		$this->load->library('session');
		$this->load->helper('url');
		$this->data=array();
		$this->data['mainmenu']=$this->mainmenu();
		$this->data['user_message']=$this->session->flashdata('user_message');
	}

	public function index($serial=false)
	{
		$this->data['title']='Index of operations';
		$filter=array();
		if($serial!==false){
			$filter['serial']=strval($serial);
		}
		else{
			$filter['related_operation_id']='';
		}
		$this->data['items'] = $this->operations_model->get_operations($filter);
		$this->data['headings']=array_merge($this->operations_model->get_headings_array(),array('',''));
		$this->load->library('table');
		$this->table->set_heading($this->data['headings']);
		$this->load->view('templates/header', $this->data);
		$this->load->view('operations/index', $this->data);
		$this->load->view('templates/footer');
	}

	public function view($id=false)
	{
		if($id===false){
			$this->session->set_flashdata('user_message','Operation is not selected');
			redirect('/operations');
			exit();
		}
		$this->data['title']=sprintf('View operation %s',$id);
		$this->data['item'] = $this->operations_model->get_operations($id);
		$this->load->view('templates/header', $this->data);
		$this->load->view('operations/view', $this->data);
		$this->load->view('templates/footer');	
	}
	
	public function add(){
		$this->data['title']='Add new operation';
		$this->prepare();
		if($this->form_validation->run()===false){
			$this->load->view('templates/header', $this->data);
			$this->load->view('operations/form', $this->data);
			$this->load->view('templates/footer');
		}
		else{
			if($this->operations_model->add_operation()){
				$this->session->set_flashdata('user_message','Operation was created successfully');
			}
			else{
				$this->session->set_flashdata('user_message','Could not create operation');
			}
			redirect('/operations');
			exit();
		}
	}
	
	public function edit($id=false){
		if($id===false){
			$this->session->set_flashdata('user_message','Operation is not selected');
			redirect('/operations');
			exit();
		}
		$this->session->set_flashdata('user_message','You cannot edit operations!');
		redirect('/operations');
		exit();
			
		$this->data['title']='Edit operation';
		$this->data['id']=$id;
		$this->prepare();
		if($this->form_validation->run()===false){
			$this->data['item'] = array_merge($this->data['item'],$this->operations_model->get_operations($id));
			$this->load->view('templates/header', $this->data);
			$this->load->view('operations/form', $this->data);
			$this->load->view('templates/footer');
		}
		else{
			if($this->operations_model->edit_operation($id)){
				$this->session->set_flashdata('user_message',sprintf('Operation %s was updated successfully',$id));
			}
			else{
				$this->session->set_flashdata('user_message',sprintf('Could not update operation %s',$id));
			}
			redirect('/operations');
			exit();
		}
	}
	
	public function delete($id=false){
		if($id===false){
			$this->data['user_message']="Operation it is not selected";
			redirect('/operations');
			exit();
		}
		$this->data['title']='Delete operation';
		if($this->operations_model->delete_operation($id)){
			$this->session->set_flashdata('user_message',sprintf("Operation %s was deleted successfully",$id));
		}
		else{
			$this->session->set_flashdata('user_message',sprintf("Could not delete operation %s",$id));
		}
		redirect('/operations');
		exit();
	}
	
	private function mainmenu(){
		return array(
			array('controller'=>'accounts','name'=>'accounts'),
			array('controller'=>'operations','name'=>'operations')
		);
	}
	
	private function prepare(){
		$this->load->helper('form');
		$this->load->library('form_validation');
		$this->form_validation->set_rules(array(
			array(
				'field'=>'serial_from',
				'label'=>'From',
				'rules'=>'required|numeric|min_lenght[20]|max_length[20]'
				),
			array(
				'field'=>'serial_to',
				'label'=>'To',
				'rules'=>'required|numeric|min_lenght[20]|max_length[20]'
				),
			array(
				'field'=>'sum',
				'label'=>'Operation sum',
				'rules'=>'required|numeric'
				),
			array(
				'field'=>'date_created',
				'label'=>'Operation date',
				'rules'=>'required'
				)
		));
		//$this->load->model('accounts_model');
		$this->data['item'] = array(
			'serial_from' => $this->input->post('serial_from'),
			'serial_to' => $this->input->post('serial_to'),
			'date_created' => (($this->input->post('date_created'))?($this->input->post('date_created')):(date('Y-m-d H:i:s',time()))),
			'sum' => $this->input->post('sum'),
			'select_accounts' => $this->operations_model->get_accounts(false, array('id','serial','client'))
		);
	}
}