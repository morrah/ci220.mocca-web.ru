<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Accounts extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('accounts_model');
		$this->load->library('session');
		$this->load->helper('url');
		$this->data=array();
		$this->data['mainmenu']=$this->mainmenu();
		$this->data['user_message']=$this->session->flashdata('user_message');
	}

	public function index()
	{
		$this->data['title']='Index of accounts';
		$this->data['items'] = $this->accounts_model->get_accounts();
		$this->data['headings']=array_merge($this->accounts_model->get_headings_array(),array('','',''));
		$this->load->library('table');
		$this->table->set_heading($this->data['headings']);
		$this->load->view('templates/header', $this->data);
		$this->load->view('accounts/index', $this->data);
		$this->load->view('templates/footer');
	}

	public function view($id=false)
	{
		if($id===false){
			$this->session->set_flashdata('user_message','Account is not selected');
			redirect('/accounts');
			exit();
		}
		$this->data['title']=sprintf('View account %s',$id);
		$this->data['item'] = $this->accounts_model->get_accounts($id);
		$this->load->view('templates/header', $this->data);
		$this->load->view('accounts/view', $this->data);
		$this->load->view('templates/footer');	
	}
	
	public function add(){
		$this->data['title']='Add new account';
		$this->prepare();
		if($this->form_validation->run()===false){
			$this->load->view('templates/header', $this->data);
			$this->load->view('accounts/form', $this->data);
			$this->load->view('templates/footer');
		}
		else{
			if($this->accounts_model->add_account()){
				$this->session->set_flashdata('user_message','Account was created successfully');
			}
			else{
				$this->session->set_flashdata('user_message','Could not create account');
			}
			redirect('/accounts');
			exit();
		}
	}
	
	public function edit($id=false){
		if($id===false){
			$this->session->set_flashdata('user_message','Account is not selected');
			redirect('/accounts');
			exit();
		}
		$this->data['title']='Edit account';
		$this->data['id']=$id;
		$this->prepare();
		if($this->form_validation->run()===false){
			$this->data['item'] = array_merge($this->data['item'],$this->accounts_model->get_accounts($id));
			$this->load->view('templates/header', $this->data);
			$this->load->view('accounts/form', $this->data);
			$this->load->view('templates/footer');
		}
		else{
			if(((string)$this->data['id']=='1')||($this->data['item']['serial']=='00000000000000000000')){
				$this->session->set_flashdata('user_message',sprintf('You cannot edit system account %s!',$id));
			}
			//$this->db->where('id', $id);
			//if($this->db->update('accounts', $this->data['item'])){
			elseif($this->accounts_model->edit_account($id)){
				$this->session->set_flashdata('user_message',sprintf('Account %s was updated successfully',$id));
			}
			else{
				$this->session->set_flashdata('user_message',sprintf('Could not update account %s',$id));
			}
			redirect('/accounts');
			exit();
		}
	}
	
	public function delete($id=false){
		if($id===false){
			$this->data['user_message']="Account it is not selected";
			redirect('/accounts');
			exit();
		}
		if((string)$id=='1'){
			$this->session->set_flashdata('user_message','You cannot delete system account');
			redirect('/accounts');
			exit();
		}
		$this->data['title']='Delete account';
		if($this->accounts_model->delete_account($id)){
			$this->session->set_flashdata('user_message',sprintf("Account %s was deleted successfully",$id));
		}
		else{
			$this->session->set_flashdata('user_message',sprintf("Could not delete account %s",$id));
		}
		redirect('/accounts');
		exit();
	}
	
	private function mainmenu(){
		return array(
			array('controller'=>'accounts','name'=>'accounts'),
			array('controller'=>'operations','name'=>'operations')
		);
	}
	
	private function prepare(){
		$this->load->helper('form');
		$this->load->library('form_validation');
		$this->form_validation->set_rules(array(
			array(
				'field'=>'client',
				'label'=>'Client name',
				'rules'=>'required|alpha_dash'
				),
			array(
				'field'=>'serial',
				'label'=>'Account serial',
				'rules'=>'required|numeric|min_lenght[20]|max_length[20]'
				),
			array(
				'field'=>'balance',
				'label'=>'Account balance',
				'rules'=>'numeric'
				)
		));
		$this->data['item'] = array(
			'client' => $this->input->post('client'),
			'serial' => (($this->input->post('serial'))?($this->input->post('serial')):($this->accounts_model->generate_serial())),
			'balance' => $this->input->post('balance')
		);
	}
}