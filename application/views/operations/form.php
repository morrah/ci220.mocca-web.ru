<?php echo validation_errors(); ?>
<?php echo form_open('operations/'.(!empty($item['id'])?'edit/'.$item['id']:'add')); ?>
	<?php if(!empty($item['id'])){
		?>
		<input type="hidden" name="id" id="id" value="<?php echo $item['id']; ?>" />
		<?php
	}
	?>
	<label for="date_created">Operation date</label>
	<input type="text" name="date_created" id="date_created" value="<?php echo !empty($item['date_created'])?$item['date_created']:''; ?>" /><br />
	
	<label for="serial_from">Operation sender serial</label>
	<select name="serial_from" id="serial_from">
		<option value="">Not selected</option>
		<?php
		foreach($item['select_accounts'] as $k=>$v){
		?>
		<option value="<?php echo $v['serial']; ?>"<?php echo ($v['serial']=='00000000000000000000'?' disabled ':(!empty($item['serial_from'])&&($item['serial_from']==$v['serial'])?' selected ':'')); ?>><?php echo $v['client'].': '.$v['serial']; ?></option>
		<?
		}
		?>
	</select><br />

	<label for="serial_to">Operation recipient serial</label>
	<select name="serial_to" id="serial_to">
		<option value="">Not selected</option>
		<?php
		foreach($item['select_accounts'] as $k=>$v){
		?>
		<option value="<?php echo $v['serial']; ?>"<?php echo ($v['serial']=='00000000000000000000'?' disabled ':(!empty($item['serial_from'])&&($item['serial_from']==$v['serial'])?' selected ':'')); ?>><?php echo $v['client'].': '.$v['serial']; ?></option>
		<?
		}
		?>
	</select><br />
	
	<label for="sum">Operation sum</label>
	<input type="text" name="sum" id="sum" value="<?php echo !empty($item['sum'])?$item['sum']:''; ?>" /><br />
	
	<input type="submit" name="submit" value="<?php echo (!empty($item['id'])?'Edit':'Add'); ?>" />
</form>