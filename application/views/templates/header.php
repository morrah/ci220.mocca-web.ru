<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="">
    <meta name="author" content="">
	<meta name="keywords" content="">
    <link rel="icon" href="/favicon.ico">

    <title><?php echo $title; ?></title>
    <link href="/css/style.css" rel="stylesheet">
    <script src="/js/jquery.1.11.1.js"></script>
</head>
<body>
	<div class="container">
		<div class="header">
			<ul>
				<li class="main"><a href="/">Home</a></li>
				<li class="main"><?php echo anchor('createAccount', 'createAccount'); ?></li>
				<li class="main"><?php echo anchor('transfer', 'transfer'); ?></li>
				<?php
				foreach($mainmenu as $k=>$v){
					?>
					<li>
						<?php echo anchor($v['controller'], $v['name']); ?>
					</li>
					<?php
				}
				?>
			</ul>
		</div>
<?php if(!empty($user_message)){
	?>
		<div class="user_mesage"><?php echo $user_message; ?></div>
	<?php
}
?>
		<div class="content">
		<h1><?php echo $title; ?></h1>