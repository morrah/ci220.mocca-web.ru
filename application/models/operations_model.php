<?php
Class Operations_Model extends CI_Model{
	public function __construct(){
		$this->load->database();
	}
	public function get_operations($filter=false){
		$this->db->order_by('date_created','desc');
		if($filter===false){
			$query=$this->db->get('operations');
			return $query->result_array();
		}
		elseif((is_array($filter))&&(!empty($filter))){
			$where_string="(1)";
			foreach($filter as $k=>$v){
				if($k=='serial'){
					$where_string.=" and ((`serial_from`='".$v."') or (`serial_to`='".$v."'))";
				}
				elseif(!empty($k)){
					$where_string.=" and (`".$k."`='".$v."')";
				}
				$this->db->where($where_string);
			}
			$query=$this->db->get('operations');
			return $query->result_array();
		}
		elseif((!empty($filter))&&((int)$filter>0)){
			$query=$this->db->get_where('operations', array('id'=>$filter));
			return $query->row_array();
		}
		
	}
	
	public function add_operation(){
		/*Данные основной операции*/
		$data = array(
			'serial_from' => $this->input->post('serial_from'),
			'serial_to' => $this->input->post('serial_to'),
			'sum' => $this->input->post('sum'),
			'date_created' => $this->input->post('date_created'),
			'related_operation_id'=>''
		);
		
		
		$this->db->trans_start();
		
		/*Начальный баланс исходного счета*/
		$from_balance=$this->db->query("select `balance` from `".$this->db->dbprefix('accounts')."` where `serial`='".$this->input->post('serial_from')."'");
		if($from_balance->num_rows>0){
			$from_balance=round(floatval($from_balance->row()->balance),2);
		}
		
		/*Начальный баланс принимающего счета*/
		$to_balance=$this->db->query("select `balance` from `".$this->db->dbprefix('accounts')."` where `serial`='".$this->input->post('serial_to')."'");
		if($to_balance->num_rows>0){
			$to_balance=round(floatval($to_balance->row()->balance),2);
		}
		
		/*Начальный баланс системного счета*/
		$system_balance=$this->db->query("select `balance` from `".$this->db->dbprefix('accounts')."` where `serial`='00000000000000000000'");
		if($system_balance->num_rows>0){
			$system_balance=round(floatval($system_balance->row()->balance),2);
		}
		
		/*Сумма комиссии*/
		$fee= round(floatval($this->input->post('sum')*0.0099),2);
		
		/*Конечный баланс исходного счета*/
		$from_balance=$from_balance-$fee-$this->input->post('sum');
		
		/*Конечный баланс принимающего счета*/
		$to_balance=$to_balance+$this->input->post('sum');
		
		/*Конечный баланс системного счета*/
		$system_balance=$system_balance+$fee;
		
		/*Основная операция (перевод средств)*/
		$this->db->insert('operations', $data);
		
		/*Данные системной операции*/
		$system_operation_data = array(
			'serial_from' => $this->input->post('serial_from'),
			'serial_to' => '00000000000000000000',
			'sum' => $fee,
			'date_created' => $this->input->post('date_created'),
			'related_operation_id'=>$this->db->insert_id()
		);
		
		/*Системная операция (перечисление комиссии)*/
		$this->db->insert('operations', $system_operation_data);
		
		/*Обновление баланса исходного счета*/
		$this->db->where('serial',$this->input->post('serial_from'));
		$this->db->update('accounts',array('balance'=>$from_balance));
		
		/*Обновление баланса принимающего счета*/
		$this->db->where('serial',$this->input->post('serial_to'));
		$this->db->update('accounts',array('balance'=>$to_balance));
		
		/*Обновление баланса системного счета*/
		$this->db->where('serial','00000000000000000000');
		$this->db->update('accounts',array('balance'=>$system_balance));
		
		$this->db->trans_complete();
		if($this->db->trans_status()===false){
			return false;
		}
		return true;
	}
	
	public function edit_operation($id=false){
		$this->session->set_flashdata('user_message','You cannot edit operations!');
		return false;
		if($id===false){
			return false;
		}
		else{
			/*Логика:
			-выбираем нужную операцию
			-выбираем связанную системную операцию
			-определяем балансы передающего, принимающего и системного счетов
			-вычисляем сбор, исходя из новой суммы операции
			-пересчитываем все балансы с учетом как ранее взятого сбора (поле 'fee' в записи системной операции), так и вновь вычисленного
			-изменяем записи основной и связанной системной операций
			-изменяем записи аккаунтов с учетом вновь пересчитанных балансов
			*/
			
			$this->db->trans_start();
			
			/*Извлекаем запись основной операции*/
			$main_operation=$this->db->get_where('operations', array('id'=>$id));
			if($main_operation->num_rows>0){
				$main_operation->row(0);
			}
			
			/*Извлекаем запись связанной системной операции*/
			$system_operation=$this->db->get_where('operations', array('related_operation_id'=>$id));
			if($system_operation->num_rows()>0){
				$system_operation->row(0);
			}
			
			/*Начальный баланс исходного счета*/
			$from_balance=$this->db->query("select `balance` from `".$this->db->dbprefix('accounts')."` where `serial`='".$main_operation('serial_from')."'");
			if($from_balance->num_rows()>0){
				$from_balance=round(floatval($from_balance->row()->balance),2);
			}
			
			/*Начальный баланс принимающего счета*/
			$to_balance=$this->db->query("select `balance` from `".$this->db->dbprefix('accounts')."` where `serial`='".$main_operation('serial_to')."'");
			if($to_balance->num_rows>0){
				$to_balance=round(floatval($to_balance->row()->balance),2);
			}
			
			/*Начальный баланс системного счета*/
			$system_balance=$this->db->query("select `balance` from `".$this->db->dbprefix('accounts')."` where `serial`='00000000000000000000'");
			if($system_balance->num_rows>0){
				$system_balance=round(floatval($system_balance->row()->balance),2);
			}
			
			/*Сумма ранее перечисленной комиссии*/
			$fee_old=round(floatval($system_operation->sum),2);
			
			/*Сумма комиссии*/
			$fee= round(floatval($this->input->post('sum')*0.0099),2);
			
			/*Конечный баланс исходного счета*/
			$from_balance=$from_balance-$fee-$this->input->post('sum');
			
			/*Конечный баланс принимающего счета*/
			$to_balance=$to_balance+$this->input->post('sum');
			
			/*Конечный баланс системного счета*/
			$system_balance=$system_balance+$fee;
			
			/*Основная операция (перевод средств)*/
			$this->db->insert('operations', $data);
			
			/*Данные системной операции*/
			$system_operation_data = array(
				'serial_from' => $this->input->post('serial_from'),
				'serial_to' => '00000000000000000000',
				'sum' => $fee,
				'date_created' => $this->input->post('date_created'),
				'related_operation_id'=>$this->db->insert_id(),
				'fee'=>$fee
			);
			
			/*Системная операция (перечисление комиссии)*/
			$this->db->insert('operations', $system_operation_data);
			
			/*Обновление баланса исходного счета*/
			$this->db->where('serial',$this->input->post('serial_from'));
			$this->db->update('accounts',array('balance'=>$from_balance));
			
			/*Обновление баланса принимающего счета*/
			$this->db->where('serial',$this->input->post('serial_to'));
			$this->db->update('accounts',array('balance'=>$to_balance));
			
			/*Обновление баланса системного счета*/
			$this->db->where('serial','00000000000000000000');
			$this->db->update('accounts',array('balance'=>$system_balance));
			
			/*Данные основной операции*/
			$data = array(
				'serial_from' => $this->input->post('serial_from'),
				'serial_to' => $this->input->post('serial_to'),
				'sum' => $this->input->post('sum'),
				'date_created' => $this->input->post('date_created')
			);
			$this->db->where('id',$id);
			return $this->db->update('operations', $data);
			
			$this->db->trans_complete();
			if($this->db->trans_status()===false){
				return false;
			}
			return true;
		}
	}
	
	public function delete_operation($id=false){
		if($id===false){
			return false;
		}
		else{
			/*Логика:
			-выбираем нужную операцию
			-выбираем связанную системную операцию
			-определяем балансы передающего, принимающего и системного счетов
			-пересчитываем все балансы с учетом ранее взятого сбора (поле 'fee' в записи системной операции)
			-удаляем записи основной и связанной системной операций
			-изменяем записи аккаунтов с учетом вновь пересчитанных балансов
			*/
			
			$this->db->trans_start();
			
			/*Извлекаем запись основной операции*/
			$main_operation=$this->db->get_where('operations', array('id'=>$id));
			if($main_operation->num_rows>0){
				$main_operation=$main_operation->row(0);
			}
			
			/*Извлекаем запись связанной системной операции*/
			$system_operation=$this->db->get_where('operations', array('related_operation_id'=>$id));
			if($system_operation->num_rows>0){
				$system_operation=$system_operation->row(0);
			}
			
			/*Начальный баланс исходного счета*/
			$from_balance=$this->db->query("select `balance` from `".$this->db->dbprefix('accounts')."` where `serial`='".$main_operation->serial_from."'");
			if($from_balance->num_rows>0){
				$from_balance=round(floatval($from_balance->row()->balance),2);
			}
			
			/*Начальный баланс принимающего счета*/
			$to_balance=$this->db->query("select `balance` from `".$this->db->dbprefix('accounts')."` where `serial`='".$main_operation->serial_to."'");
			if($to_balance->num_rows>0){
				$to_balance=round(floatval($to_balance->row()->balance),2);
			}
			
			/*Начальный баланс системного счета*/
			$system_balance=$this->db->query("select `balance` from `".$this->db->dbprefix('accounts')."` where `serial`='00000000000000000000'");
			if($system_balance->num_rows>0){
				$system_balance=round(floatval($system_balance->row()->balance),2);
			}
			
			/*Сумма ранее перечисленной комиссии*/
			$fee=round(floatval($system_operation->sum),2);
			
			/*Конечный баланс исходного счета*/
			$from_balance=$from_balance+$fee+$main_operation->sum;
			
			/*Конечный баланс принимающего счета*/
			$to_balance=$to_balance-$main_operation->sum;
			
			/*Конечный баланс системного счета*/
			$system_balance=$system_balance-$fee;
			
			/*Удаление операций (Основной и системной)*/
			$this->db->query("delete from `".$this->db->dbprefix('operations')."` where `id` in('".$main_operation->id."','".$system_operation->id."')");
			
			/*Обновление баланса исходного счета*/
			$this->db->where('serial',$main_operation->serial_from);
			$this->db->update('accounts',array('balance'=>$from_balance));
			
			/*Обновление баланса принимающего счета*/
			$this->db->where('serial',$main_operation->serial_to);
			$this->db->update('accounts',array('balance'=>$to_balance));
			
			/*Обновление баланса системного счета*/
			$this->db->where('serial','00000000000000000000');
			$this->db->update('accounts',array('balance'=>$system_balance));
			
			$this->db->trans_complete();
			if($this->db->trans_status()===false){
				return false;
			}
			return true;
		}
	}
	
	public function generate_date_created(){
		return strval(time());
	}
	public function get_headings(){
		$query=$this->db->query("describe `".$this->db->dbprefix('operations')."`");
		return $query->result_array();
	}
	public function get_headings_array(){
		$headings_array=$this->get_headings();
		if(($headings_array)&&(count($headings_array)>0)){
			foreach($headings_array as $k=>$v){
				$headings_array[$k]=$headings_array[$k]['Field'];
			}
		}
		return $headings_array;
	}
	public function get_accounts(){
		$this->load->model('accounts_model');
		return $this->accounts_model->get_accounts(false, array('id','serial','client'));
	}
}