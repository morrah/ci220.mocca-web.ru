<?php
Class Accounts_Model extends CI_Model{
	public function __construct(){
		$this->load->database();
	}
	public function get_accounts($filter=false,$fields=false){
		if(is_array($fields)){
			$fields_sql='';
			foreach($fields as $k=>$v){
				$fields_sql.='`'.(string)$v.'`';
				if($k<count($fields)){
					$fields_sql.=',';
				}
			}
			$this->db->select($fields_sql,false);
		}
		if($filter===false){
			$query=$this->db->get('accounts');
			return $query->result_array();
		}
		elseif((is_array($filter))&&(!empty($filter))){
			$filter_array=array();
			foreach($filter as $k=>$v){
				$filter_array[$k]=$v;
			}
			$query=$this->db->get_where('accounts', $filter_array);
			return $query->result_array();
		}
		elseif((!empty($filter))&&((int)$filter>0)){
			$query=$this->db->get_where('accounts', array('id'=>$filter));
			return $query->row_array();
		}
		
	}
	
	public function add_account(){
		$data = array(
			'client' => $this->input->post('client'),
			'serial' => $this->input->post('serial'),
			'balance' => $this->input->post('balance')
		);
		return $this->db->insert('accounts', $data);
	}
	
	public function edit_account($id=false){
		if($id===false){
			return false;
		}
		else{
			$data = array(
				'client' => $this->input->post('client'),
				'serial' => $this->input->post('serial'),
				'balance' => $this->input->post('balance')
			);
			$this->db->where('id',$id);
			return $this->db->update('accounts', $data);
		}
	}
	
	public function delete_account($id=false){
		if($id===false){
			return false;
		}
		else{
			return $this->db->delete('accounts',array('id'=>$id));
		}
	}
	
	public function generate_serial(){
		$query=$this->db->query("select max(`serial`) as 'serial' from `".$this->db->dbprefix('accounts')."`");
		if ($query->num_rows() > 0){
			$new_serial=$query->row()->serial;
			$new_serial=(int)$new_serial+1;
			$new_serial=strval(sprintf('%020d',(string)$new_serial));
		}
		else{
			$new_serial='00000000000000000001';
		}
		return $new_serial;
	}
	public function get_headings(){
		$query=$this->db->query("describe `".$this->db->dbprefix('accounts')."`");
		return $query->result_array();
	}
	public function get_headings_array(){
		$headings_array=$this->get_headings();
		if(($headings_array)&&(count($headings_array)>0)){
			foreach($headings_array as $k=>$v){
				$headings_array[$k]=$headings_array[$k]['Field'];
			}
		}
		return $headings_array;
	}
}