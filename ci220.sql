-- phpMyAdmin SQL Dump
-- version 3.5.1
-- http://www.phpmyadmin.net
--
-- Хост: 127.0.0.1
-- Время создания: Июл 24 2014 г., 21:06
-- Версия сервера: 5.5.25
-- Версия PHP: 5.3.13

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `ci220`
--

-- --------------------------------------------------------

--
-- Структура таблицы `ci220_accounts`
--

CREATE TABLE IF NOT EXISTS `ci220_accounts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `client` varchar(250) NOT NULL,
  `serial` varchar(20) NOT NULL,
  `balance` double(15,2) NOT NULL DEFAULT '0.00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `serial` (`serial`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='Счета клиентов' AUTO_INCREMENT=4 ;

--
-- Дамп данных таблицы `ci220_accounts`
--

INSERT INTO `ci220_accounts` (`id`, `client`, `serial`, `balance`) VALUES
(1, 'System', '00000000000000000000', 0.65),
(2, 'morrah', '00000000000000000001', 34.25),
(3, 'vasya', '00000000000000000002', 68.10);

-- --------------------------------------------------------

--
-- Структура таблицы `ci220_operations`
--

CREATE TABLE IF NOT EXISTS `ci220_operations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `related_operation_id` int(11) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `serial_from` varchar(20) NOT NULL,
  `serial_to` varchar(20) NOT NULL,
  `sum` double(15,2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='Операции по счетам клиентов' AUTO_INCREMENT=19 ;

--
-- Дамп данных таблицы `ci220_operations`
--

INSERT INTO `ci220_operations` (`id`, `related_operation_id`, `date_created`, `serial_from`, `serial_to`, `sum`) VALUES
(13, 0, '2014-07-24 13:50:23', '00000000000000000001', '00000000000000000002', 15.00),
(14, 13, '2014-07-24 13:50:23', '00000000000000000001', '00000000000000000000', 0.15),
(15, 0, '2014-07-24 13:51:49', '00000000000000000001', '00000000000000000002', 25.00),
(16, 15, '2014-07-24 13:51:49', '00000000000000000001', '00000000000000000000', 0.25),
(17, 0, '2014-07-24 13:52:10', '00000000000000000001', '00000000000000000002', 25.10),
(18, 17, '2014-07-24 13:52:10', '00000000000000000001', '00000000000000000000', 0.25);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
